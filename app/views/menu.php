
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Contagios COVID19</title>
    <link rel="stylesheet" href="public/css/estilos.css">
</head>
<body>
<header>
    <nav>
        <a href="#">Inicio</a>
        <a href="#">acerca de</a>
        <a href="#">estados con mas contaguios</a>
        <a href="#">normas de seguridad</a>
        <a href="#">contacto</a>
    </nav>
    <section class="textos-header">
        <h1>Estados con más contagios por semana</h1>
        <h2>Información pública para la nación</h2>
    </section>
    <div class="wave" style="height: 150px; overflow: hidden;" ><svg viewBox="0 0 500 150" preserveAspectRatio="none" style="height: 100%; width: 100%;"><path d="M-9.31,88.31 C242.37,151.47 261.57,-98.19 511.57,76.47 L511.57,170.22 L0.00,150.00 Z" style="stroke: none; fill: #fff;"></path></svg></div>
</header>
<main>
    <section class="contenedor sobre-nosotros">
        <h2 class="titulo">Mapa de mexico</h2>
        <div class="contenedor-sobre-nosotros">
            <img src="public/images/img2.jpg" alt="" class="imagen-about-us">
            <div class="contenido-texto">
                <h3><span>1</span>Color verde indica zona segura de Covid-19</h3>
                <h3><span>2</span>Color naranja indica posibilidad de contagios</h3>
                <h3><span>3</span>Color rojo peligro de contagios de Covid-19</h3>
            </div>
        </div>
    </section>
    <section class="portafolio">
        <div class="contenedor">
            <h2 class="titulo">Estados con mas contagios</h2>
            <div class="galeria-port">
                <div class="imagen-port">
                    <img src="public/images/imagen1.jpg" alt="">
                    <div class="hover-galeria">
                        <img src="public/images/clic.jpg" alt="">
                        <p>CDMX</p>
                    </div>
                </div>
                <div class="imagen-port">
                    <img src="public/images/imagen2.jpg" alt="">
                    <div class="hover-galeria">
                        <img src="public/images/clic.jpg" alt="">
                        <p>Estado de México</p>
                    </div>
                </div>
                <div class="imagen-port">
                    <img src="public/images/imagen3.jpg" alt="">
                    <div class="hover-galeria">
                        <img src="public/images/clic.jpg" alt="">
                        <p>Quintana Roo</p>
                    </div>
                </div>
                <div class="imagen-port">
                    <img src="public/images/imagen4.jpg" alt="">
                    <div class="hover-galeria">
                        <img src="public/images/clic.jpg" alt="">
                        <p>Puebla</p>
                    </div>
                </div>
                <div class="imagen-port">
                    <img src="public/images/imagen5.jpg" alt="">
                    <div class="hover-galeria">
                        <img src="public/images/clic.jpg" alt="">
                        <p>Sinaloa</p>
                    </div>
                </div>
                <div class="imagen-port">
                    <img src="public/images/imagen6.jpg" alt="">
                    <div class="hover-galeria">
                        <img src="public/images/clic.jpg" alt="">
                        <p>Baja California</p>
                    </div>
                </div>
                <div class="imagen-port">
                    <img src="public/images/imagen7.jpg" alt="">
                    <div class="hover-galeria">
                        <img src="public/images/clic.jpg" alt="">
                        <p>Tabasco</p>
                    </div>
                </div>
                <div class="imagen-port">
                    <img src="public/images/imagen8.jpg" alt="">
                    <div class="hover-galeria">
                        <img src="public/images/clic.jpg" alt="">
                        <p>Coahuila</p>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section  class="tablas" >
        <h1>Tabla de información general acerca de los contagios</h1>
        <form method="post" action="index.php?controller=Estados&action=mostrar">
        <table class="table" >
            <tr>
                <th>Estados</th>
                <th>Semanas</th>
                <th>Contagios</th>
                <th>Contagios por hombre</th>
                <th>Contagios por mujer</th>
                <th>Edad Promedio</th>
            </tr>
            <tbody id="datos">

                <tr>
                    <td>CDMX</td>
                    <td>
                        <input type="number" placeholder="n° Semana" name="semana">
                    </td>
                    <td><? $row["poblacion"]; ?></td>
                    <td><? $row["poblaHom"]; ?></td>
                    <td><? $row["poblaMuj"]; ?></td>
                    <td><? $row["edadProm"]; ?></td>
                </tr>



            </tbody>

            <tbody id="datos">

                <tr>
                    <td>Edo mex</td>
                    <td>
                        <input type="number" placeholder="n° Semana" name="semana">
                    </td>

                </tr>


            </tbody>
            <tr>
                <td>Baja California</td>
                <td>
                    <input type="number" placeholder="n° Semana" name="semana">
                </td>
                <td><? $row["poblacion"]; ?></td>
                <td><? $row["poblaHom"]; ?></td>
                <td><? $row["poblaMuj"]; ?></td>
                <td><? $row["edadProm"]; ?></td>
            </tr>
            <tr>
                <td>Sinaloa</td>
                <td>
                    <input type="number" placeholder="n° Semana" name="semana">
                </td>
                <td><? $row["poblacion"]; ?></td>
                <td><? $row["poblaHom"]; ?></td>
                <td><? $row["poblaMuj"]; ?></td>
                <td><? $row["edadProm"]; ?></td>
            </tr>
            <tr>
                <td>Puebla</td>
                <td>
                    <input type="number" placeholder="n° Semana" name="semana">
                </td>
                <td><? $row["poblacion"]; ?></td>
                <td><? $row["poblaHom"]; ?></td>
                <td><? $row["poblaMuj"]; ?></td>
                <td><? $row["edadProm"]; ?></td>
            </tr>
            <tr>
                <td>Tabasco</td>
                <td>
                    <input type="number" placeholder="n° Semana" name="semana">
                </td>
                <td><? $row["poblacion"]; ?></td>
                <td><? $row["poblaHom"]; ?></td>
                <td><? $row["poblaMuj"]; ?></td>
                <td><? $row["edadProm"]; ?></td>
            </tr>
            <tr>
                <td>Quintana Roo</td>
                <td>
                    <input type="number" placeholder="n° Semana" name="semana">
                </td>
                <td><? $row["poblacion"]; ?></td>
                <td><? $row["poblaHom"]; ?></td>
                <td><? $row["poblaMuj"]; ?></td>
                <td><? $row["edadProm"]; ?></td>
            </tr>
            <tr>
                <td>Coahuila</td>
                <td>
                    <input type="number" placeholder="n° Semana" name="semana">
                </td>
                <td><? $row["poblacion"]; ?></td>
                <td><? $row["poblaHom"]; ?></td>
                <td><? $row["poblaMuj"]; ?></td>
                <td><? $row["edadProm"]; ?></td>
            </tr>

        </table>
        <input type="submit" value="Mostrar" class="button" name="registrar">
        </form>

        <div class="cantainer-comparar">
            <button class="btn1">comparar</button>
        </div>
    </section>
    <section class="clientes contenedor">
        <h2 class="titulo">que disen nuestros doctores</h2>
        <div class="cards">
            <div class="card">
                <img src="img/person1.jpg" alt="">
                <div class="contenedor-texto-card">
                    <h4>name</h4>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Doloremque, laudantium.</p>
                </div>
            </div>
            <div class="card">
                <img src="img/person2.jpg" alt="">
                <div class="contenedor-texto-card">
                    <h4>name</h4>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Doloremque, laudantium.</p>
                </div>
            </div>
        </div>
    </section>
    <section class="about-services">
        <div class="contenedor">
            <h2 class="titulo">prevenciones de contaguios</h2>
            <div class="servicio-cont">
                <div class="servicio-ind">
                    <img src="img/img3.jpg" alt="">
                    <h3>name</h3>
                    <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Non, accusamus.</p>
                </div>
                <div class="servicio-ind">
                    <img src="img/img3.jpg" alt="">
                    <h3>name</h3>
                    <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Non, accusamus.</p>
                </div>
                <div class="servicio-ind">
                    <img src="img/img3.jpg" alt="">
                    <h3>name</h3>
                    <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Non, accusamus.</p>
                </div>
            </div>
        </div>
    </section>
</main>
<footer>
    <div class="contenedor-footer">
        <div class="content-foo">
            <h4>phone</h4>
            <p>57567467</p>
        </div>
        <div class="content-foo">
            <h4>email</h4>
            <p>57567467</p>
        </div>
        <div class="content-foo">
            <h4>localitation</h4>
            <p>57567467</p>
        </div>
    </div>
    <h2 class="titulo-final">&copy;eddy y raul </h2>
</footer>

</body>
</html>
