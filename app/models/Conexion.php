<?php


namespace UPT;


class Conexion
{
    public $con;

    public function __construct()
    {

        $host="localhost";
        $user = "root";
        $pass = "";
        $bd = "edosContagios";

        $this->con = mysqli_connect($host, $user, $pass, $bd);
        mysqli_query($this->con, "SET NAMES 'utf8");
    }
}